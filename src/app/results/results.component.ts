import {Component, Input, OnInit} from '@angular/core';
import * as Chartist from 'chartist';
import {Router} from '@angular/router';
import {Model} from '../models/model';
import {Utils} from '../utils/Utils';
import {Result} from '../models/result';
import {IBarChartOptions} from 'chartist';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  imgURL: any;
  modelToUse: Model;
  modelUrl: string;
  dataResults: Result;

  constructor(public router: Router) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.dataResults = this.router.getCurrentNavigation().extras.state.results;
      this.imgURL = this.router.getCurrentNavigation().extras.state.imgURL;
    }
  }
  startAnimationForLineChart(chart){
    let seq: any, delays: any, durations: any;
    seq = 0;
    delays = 80;
    durations = 500;

    chart.on('draw', function(data) {
      if(data.type === 'line' || data.type === 'area') {
        data.element.animate({
          d: {
            begin: 600,
            dur: 700,
            from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
            to: data.path.clone().stringify(),
            easing: Chartist.Svg.Easing.easeOutQuint
          }
        });
      } else if(data.type === 'point') {
        seq++;
        data.element.animate({
          opacity: {
            begin: seq * delays,
            dur: durations,
            from: 0,
            to: 1,
            easing: 'ease'
          }
        });
      }
    });

    seq = 0;
  };
  startAnimationForBarChart(chart){
    let seq2: any, delays2: any, durations2: any;

    seq2 = 0;
    delays2 = 80;
    durations2 = 500;
    chart.on('draw', function(data) {
      if(data.type === 'bar'){
        seq2++;
        data.element.animate({
          opacity: {
            begin: seq2 * delays2,
            dur: durations2,
            from: 0,
            to: 1,
            easing: 'ease'
          }
        });
      }
    });

    seq2 = 0;
  };



  ngOnInit() {
    /* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */

    const dataResultsChart: any = {
      // labels: ['0', '40', '80', '120', '160', '200', '240'],
      // series: [
      //   [5, 8, 13, 17, 23, 15, 9]
      // ]
      labels: [],
      series: [[]]
    };
    this.dataResults.results.forEach((category) => {
      dataResultsChart.labels.push(category.name)
      dataResultsChart.series[0].push(parseInt(category.score.toString().replace('%', '')))
      // dataResultsChart.series[0].push(category.score)
    })

    console.log('dataResultsChart ', dataResultsChart);

    const optionsResultsChart: any = {
      // lineSmooth: Chartist.Interpolation.cardinal({
      //   tension: 0
      // }),
      // low: 0,
      // high: 50, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
      // chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
      axisX: {
        showGrid: false
      },
      low: 0,
      high: 100,
      chartPadding: { top: 0, right: 5, bottom: 0, left: 0}
    }

    const responsiveOptions: any[] = [
      ['screen and (max-width: 640px)', {
        seriesBarDistance: 4,
        distributeSeries: true,
        labelOffset: {
          x: 13
        },
        axisX: {
          labelOffset: {
            x: 130
          }
        }
      }]
    ];

    const dataChartOption: IBarChartOptions = {
      height: 200,
      seriesBarDistance: 4,
      axisX: {
        labelOffset: {
          x: 160
        }
      }
    }
    const results = new Chartist.Bar('#dataResults', dataResultsChart, dataChartOption, responsiveOptions);

    this.startAnimationForLineChart(results);


    /* ----------==========     Completed Tasks Chart initialization    ==========---------- */

    const dataCompletedTasksChart: any = {
      labels: ['12p', '3p', '6p', '9p', '12p', '3a', '6a', '9a'],
      series: [
        [230, 750, 450, 300, 280, 240, 200, 100]
      ]
    };

    const optionsCompletedTasksChart: any = {
      lineSmooth: Chartist.Interpolation.cardinal({
        tension: 0
      }),
      low: 0,
      high: 1000, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
      chartPadding: { top: 0, right: 0, bottom: 0, left: 0}
    }

    var completedTasksChart = new Chartist.Line('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);

    // start animation for the Completed Tasks Chart - Line Chart
    this.startAnimationForLineChart(completedTasksChart);
  }

}
