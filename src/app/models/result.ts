export class Result {
    results: CategoryResult[];
}

export class CategoryResult {
    name: string;
    score: number;
    accepted: boolean;
}
