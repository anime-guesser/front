export enum Model {
    linear= 'linear',
    mlp = 'mlp',
    cnn = 'cnn',
    rnn = 'rnn'
}
