import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadAnimePosterComponent } from './upload-anime-poster.component';

describe('UploadAnimePosterComponent', () => {
  let component: UploadAnimePosterComponent;
  let fixture: ComponentFixture<UploadAnimePosterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadAnimePosterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadAnimePosterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
