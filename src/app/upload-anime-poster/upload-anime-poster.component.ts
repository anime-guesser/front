import { Component, OnInit } from '@angular/core';
import {NavigationExtras, Router} from '@angular/router';
import {AnimeDataService} from '../services/anime-data.service';
import {error} from '@angular/compiler/src/util';
import {Utils} from '../utils/Utils';
import {Post} from '../models/post';
import {Location} from '@angular/common';

@Component({
  selector: 'app-upload-anime-poster',
  templateUrl: './upload-anime-poster.component.html',
  styleUrls: ['./upload-anime-poster.component.css']
})
export class UploadAnimePosterComponent implements OnInit {
  private uploadedFiles: any[] = [];
  public imagePath;
  imgURL: any;
  public message: string;

  constructor(public router: Router, private animeDataService: AnimeDataService, location: Location) { }

  ngOnInit(): void {
  }

  upload(filesImage, filesModel) {
    const formData = new FormData();
    // const data = new Post();
    // for (let i = 0; i < this.uploadedFiles.length; i++) {
    //   formData.append('file', this.uploadedFiles[i], this.uploadedFiles[i].name);
    //   formData.append('type', Utils.convertUrlToModel(this.router.url));
    // }
    if (filesImage.length > 0) {
      formData.append('file', filesImage[0]);
    }
    if (filesModel.length > 0) {
      formData.append('model', filesModel[0]);
    }
    formData.append('type', Utils.convertUrlToModel(location.hash));
    // data.file = files[0];
    // data.type = Utils.convertUrlToModel(this.router.url);
    this.animeDataService.postPoster(formData).subscribe((res) => {
      console.log(res);
      // TODO : navigate to /dataResults and pass responses as parameters
      this.router.navigate([location.hash.substring(2) +  '/results'], {
        state: {results: res, imgURL: this.imgURL}
      });
    },
        err => {
          console.log(err);
        }
    );
  }

  preview(files) {
    this.uploadedFiles = files;
    if (files.length === 0) {
      return;
    }

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.message = 'Only images are supported.';
      return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }
}
