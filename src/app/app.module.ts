import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';


import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import {
  AgmCoreModule
} from '@agm/core';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { UploadAnimePosterComponent } from './upload-anime-poster/upload-anime-poster.component';
import {ResultsComponent} from './results/results.component';
import {HttpClientModule} from '@angular/common/http';
import {AdminLayoutModule} from './layouts/admin-layout/admin-layout.module';
@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    ResultsComponent,
    UploadAnimePosterComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
