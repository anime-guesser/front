import {Model} from '../models/model';

export class Utils {
    static convertUrlToModel(url: string) {
        url = url.substring(2);
        console.log(url)
        switch (url) {
            case 'linear-model':
                return Model.linear;
            case 'neural-network-model':
                return Model.mlp;
            case 'convolutional-neural-network':
                return Model.cnn;
            case 'residualneural-network':
                return Model.rnn;
            default:
        }

    }
}
