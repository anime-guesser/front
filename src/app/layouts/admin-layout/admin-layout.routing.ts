import { Routes } from '@angular/router';

import { DashboardComponent } from '../../dashboard/dashboard.component';
import {UploadAnimePosterComponent } from '../../upload-anime-poster/upload-anime-poster.component';
import {ResultsComponent} from '../../results/results.component';

export const AdminLayoutRoutes: Routes = [
    // { path: 'dashboard',      component: DashboardComponent },
    // {
    //     path: 'anime-poster',
    //     component: UploadAnimePosterComponent,
    //     children: [
    //         {
    //             path: 'linear-model',
    //             component: ResultsComponent,
    //             children: [
    //                 {
    //                     path: 'results',
    //                     component: ResultsComponent,
    //                 }
    //             ]
    //         },
    //         {
    //             path: 'neural-network-model',
    //             component: UploadAnimePosterComponent,
    //             children: [
    //                 {
    //                     path: 'results',
    //                     component: ResultsComponent
    //                 }
    //             ]
    //         },
    //         {
    //             path: 'convolutional-neural-network',
    //             component: UploadAnimePosterComponent,
    //             children: [
    //                 {
    //                     path: 'results',
    //                     component: ResultsComponent
    //                 }
    //             ]
    //         },
    //         {
    //             path: 'residualneural-network',
    //             component: UploadAnimePosterComponent,
    //             children: [
    //                 {
    //                     path: 'results',
    //                     component: ResultsComponent
    //                 }
    //             ]
    //         }
    // ]},
];
