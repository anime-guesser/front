import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Result} from '../models/result';
import {Post} from '../models/post';

@Injectable({
  providedIn: 'root'
})
export class AnimeDataService {
  apiURL = 'http://localhost:5000/'

  constructor(private http: HttpClient) { }

  postPoster(data: FormData): Observable<Result> {
    return this.http.post<Result>(this.apiURL, data);
  }
}
