import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {UploadAnimePosterComponent} from './upload-anime-poster/upload-anime-poster.component';
import {ResultsComponent} from './results/results.component';
import {AppComponent} from './app.component';

const routes: Routes = [
  { path: 'dashboard',      component: DashboardComponent },
  // {
  //   path: 'anime-poster',
  //   component: UploadAnimePosterComponent,
  //   children: [
  //
  //   ]},
  {
    path: 'linear-model',
    component: UploadAnimePosterComponent,
    children: [
      {
        path: 'results',
        component: ResultsComponent,
      }
    ]
  },
  {
    path: 'neural-network-model',
    component: UploadAnimePosterComponent,
    children: [
      {
        path: 'results',
        component: ResultsComponent
      }
    ]
  },
  {
    path: 'convolutional-neural-network',
    component: UploadAnimePosterComponent,
    children: [
      {
        path: 'results',
        component: ResultsComponent
      }
    ]
  },
  {
    path: 'residualneural-network',
    component: UploadAnimePosterComponent,
    children: [
      {
        path: 'results',
        component: ResultsComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
       useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
