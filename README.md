# Anime Guesser Web Interface

## Prérequis

Installer les dépendances :

```shell script
npm install
```

## Démarrer l'application

```
ng serve
```
L'application démarre sur le port 4200 par défaut

## Utilisation

Une fois arrivé sur la page d'acceuil, cliquer sur "Dashboard" dans la barre de navigation à gauche :

![Dashboard](./src/assets/img/dashboard.png)

Puis choisir un modèle à tester. Ensuite la page suivante nous propose d'upload une affiche à tester :

![Page d'upload](./src/assets/img/upload.png)

On peut choisir de charger juste une image à tester ou aussi un modèle pré-entrainés, voici les règles à respecter :

* les images doivent être au format : ```png / jpeg / PNG / jpg```
* les fichiers keras doivent être nommés de cette façon :
     - Linear model : ``linear.keras``
     - Neural Network Model : ``mlp.keras``
     - Convolutional Neural Network : ``cnn.keras``
     - Residual Neural Network : ``rnn.keras``
